// Функции

/*
  Напишите функцию, которая подсчитывает, сколько раз встречается заданное число в заданном массиве. Напишите тестовую функцию, чтобы проверить правильность работы функции.
 */


function countQuantityOfNumberInArray() {
  const str = jsConsole.read('#str1');
  const initialArray = str.replace(/\s+/g,'').split(',').map(function (item) {
    return parseInt(item)
  });
  const numberForSearch = jsConsole.readInteger('#str2');
  const result = initialArray.filter(function (item) {
    return item === numberForSearch;
  }).length;
  jsConsole.writeLine(result);
};