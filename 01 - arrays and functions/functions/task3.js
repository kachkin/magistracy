// Функции

/*
  Напишите функцию, которая находит все вхождения слова в текст
  Поиск с учетом регистра или без учета регистра
  Используйте перегрузку функций
 */
function searchWord() {
  const text = jsConsole.read('#text');
  const word = jsConsole.read('#word');
  function findSubstring (str) {
    const resultArray = str.match(new RegExp(`${word}\\b`, "gmi"));
    return resultArray ? resultArray.length : 0;
  };
  const result = findSubstring(text);
  if (result === 0) {
    jsConsole.writeLine(`There isn\'t the word ${word}`)
  } else if (result === 1) {
    jsConsole.writeLine(`There is 1 occurrence of the word ${word}`)
  } else {
    jsConsole.writeLine(`There are ${result} occurrences of the word ${word}`)
  }
};