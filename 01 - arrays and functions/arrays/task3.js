// Массивы

/*
   Напишите скрипт, который найдет максимальную последовательность равных элементов в массиве.
 */

function findMaxSequence () {
  const str = jsConsole.read('#str');
  const array = str.replace(/\s+/g,'').split(',');
  jsConsole.writeLine(`The input sequence is: <br> ${array.join(' ')}<br>`);
  let result = [array[0]];
  let checkArray = [];
  for (let i = 1; i < array.length; i++) {
    if (array[i] === array[i - 1]) {
      if (array[i] === result[0]) {
        result.push(array[i])
      } else if (array[i] === checkArray[0]) {
        checkArray.push(array[i]);
        if (result.length < checkArray.length) {
          result = checkArray;
          checkArray = [];
        }
      }
    } else {
      checkArray = [array[i]];
    }

  }
  jsConsole.writeLine(`The maximum sequence is: <br> ${result.join(' ')}`);
}

