// Массивы

/*
  1. Написать скрипт, который выделяет массив из 20 целых чисел и инициализирует каждый элемент по его индексу, умноженному на 5. Выведите полученный массив на консоль.
 */

function createArray() {
  let array = new Array(20);
  let result = ''
  for (let index = 0; index < array.length; index++) {
    result = `${result}${index * 5}<br> `;
  }
  jsConsole.writeLine(result
  );
}