// Массивы

/*
   Напишите программу, которая найдет наиболее частое число в массиве.
 */


function findMostFrequent () {
  const str = jsConsole.read('#str');
  const array = str.replace(/\s+/g,'').split(',').map(function (item) {
    return parseInt(item)
  });
  array.sort(function(a, b) { return a - b });
  let result = [array[0]];
  let checkArray = [];
  for (let i = 1; i < array.length; i++) {
    if (array[i] === array[i - 1]) {
      if (array[i] === result[0]) {
        result.push(array[i])
      } else if (array[i] === checkArray[0]) {
        checkArray.push(array[i]);
        if (result.length < checkArray.length) {
          result = checkArray;
          checkArray = [];
        }
      }
    } else {
      checkArray = [array[i]];
    }

  }
  jsConsole.writeLine(`${result[0]} (${result.length} times)`);
}