
function parseURL() {
  try {
    const str = jsConsole.read('#str');
    const url = str ? new URL(str) : window.location;
    const result = {
      protocol: url.protocol,
      server: url.hostname,
      resource: url.pathname,
    };
    jsConsole.writeLine(JSON.stringify(result, null, 2));
    return result;
  } catch (e) {
    jsConsole.writeLine(e)
  }
}